from flask import Flask, request, jsonify, make_response
from flask_cors import cross_origin
import gevent.pywsgi as pywsgi
import gevent.monkey as monkey

monkey.patch_all()
app = Flask("flask_ajax")
app.secret_key = "1234567890"


@app.route("/api/get_provinces", methods=["GET"])
@cross_origin()
def get_provinces():
    provinces = [{"province_code": "hubei", "province_name": "湖北"}]
    result = jsonify(code=0, message="success", data=provinces)
    headers = {
        "Content-Type": "application/json;charset=utf-8"
    }
    response = make_response(result, 200, headers)
    return response


@app.route("/api/get_citys", methods=["GET"])
@cross_origin()
def get_citys():
    province_code = request.args.get("province_code", "")
    if len(province_code) == 0:
        result = jsonify(code=101, message="fail")
    else:
        citys = [{"city_code": "wuhan", "city_name": "武汉"}, {"city_code": "huangshi", "city_name": "黄石"}]
        result = jsonify(code=0, message="success", data=citys)
    headers = {
        "Content-Type": "application/json;charset=utf-8"
    }
    response = make_response(result, 200, headers)
    return response


@app.route("/api/get_shops", methods=["POST"])
@cross_origin()
def get_shops():
    city_code = request.json.get("city_code", "")
    if len(city_code) == 0:
        result = jsonify(code=101, message="fail")
    else:
        shops = [{"shop_code": "S001", "shop_name": "汉口武商广场店"}, {"shop_code": "S002", "shop_name": "武昌杨汊湖店"}]
        result = jsonify(code=0, message="success", data=shops)
    headers = {
        "Content-Type": "application/json;charset=utf-8"
    }
    response = make_response(result, 200, headers)
    return response


if __name__ == '__main__':
    # app.run(host="0.0.0.0", port=8000, threaded=True, debug=False)
    gevent_server = pywsgi.WSGIServer(('0.0.0.0', 8000), app)
    gevent_server.serve_forever()
