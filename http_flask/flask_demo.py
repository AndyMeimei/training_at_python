import random, datetime
from flask import Flask, request, make_response, jsonify, send_file
from flask import render_template, redirect, url_for, session
from werkzeug.utils import secure_filename
from flask_wtf.csrf import CSRFProtect
from flasgger import Swagger, swag_from


def get_id(n):
    v = random.randint(pow(10, n - 1), pow(10, n) - 1)
    return str(v)


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


app = Flask("flask_demo")
app.secret_key = get_id(16)
app.config["UPLOAD_FOLDER"] = "d:/data"
csrf = CSRFProtect()
swagger_config = Swagger.DEFAULT_CONFIG
swagger_config["title"] = "演示接口文档"
swagger_config["description"] = "这是一份演示用接口文档"
Swagger(app, config=swagger_config)


@app.route("/api/demo_get", methods=["GET"])
@swag_from("swagger/get_code1.yml")
def demo_get():
    app.logger.warning("发起请求(GET)：/api/demo_get")
    code = request.args.get("code", "")
    if code == "error":
        raise Exception("系统异常错误")
    result = "code = " + code
    response = make_response(result)
    return response


@app.route("/api/demo_post", methods=["POST"])
@csrf.exempt
def demo_post():
    code = request.form.get("code")
    result = "code = " + code
    response = make_response(result)
    return response


@app.route("/api/demo_json", methods=["POST"])
@csrf.exempt
def demo_json():
    code = request.json.get("code")
    result = jsonify(code=code)
    response = make_response(result)
    return response


@app.route("/api/demo_upload", methods=["POST"])
@csrf.exempt
def demo_upload():
    test_file = request.files.get("test_file")
    path = "D:/lab/python/pycharm/demo_flask/static"
    upload_ext = test_file.filename.split(".")[-1]
    upload_name = get_seq() + get_id(3) + "." + upload_ext
    upload_path = path + "/" + secure_filename(upload_name)
    test_file.save(upload_path)
    return "success"


@app.route("/api/demo_download", methods=["GET"])
def demo_download():
    path = "d:/data/test1.txt"
    download_ext = path.split(".")[-1]
    download_name = get_seq() + get_id(3) + "." + download_ext
    response = send_file(path, as_attachment=True, attachment_filename=download_name)
    return response


@app.route("/api/demo_template", methods=["GET"])
def demo_template():
    code = request.args.get("code")
    result = {"code": code}
    template = render_template("flask_template.html", result=result)
    return template


@app.route("/api/demo_redirect1", methods=["GET"])
def demo_redirect1():
    code = request.args.get("code")
    url = url_for("demo_redirect2", code=code)
    response = redirect(url)
    return response


@app.route("/api/demo_redirect2", methods=["GET"])
def demo_redirect2():
    code = request.args.get("code")
    url = url_for("demo_get", code=code)
    response = redirect(url)
    return response


@app.route("/api/demo_cookie_get", methods=["GET"])
def demo_cookie_get():
    c_id = request.cookies.get("c_id")
    if c_id is None:
        c_id = ""
    result = "get(c_id) = " + c_id
    response = make_response(result)
    return response


@app.route("/api/demo_cookie_set", methods=["GET"])
def demo_cookie_set():
    c_id = get_id(8)
    result = "set(c_id) = " + c_id
    response = make_response(result)
    response.set_cookie("c_id", c_id, max_age=3600)
    return response


@app.route("/api/demo_cookie_reset", methods=["GET"])
def demo_cookie_reset():
    result = "reset cookie"
    response = make_response(result)
    response.delete_cookie("c_id")
    return response


@app.route("/api/demo_session_get", methods=["GET"])
def demo_session_get():
    s_id = session.get("s_id")
    if s_id is None:
        s_id = ""
    result = "get(s_id) = " + s_id
    response = make_response(result)
    return response


@app.route("/api/demo_session_set", methods=["GET"])
def demo_session_set():
    s_id = get_id(8)
    result = "set(s_id) = " + s_id
    response = make_response(result)
    session["s_id"] = s_id
    return response


@app.route("/api/demo_session_reset", methods=["GET"])
def demo_session_reset():
    result = "reset session"
    response = make_response(result)
    session.pop("s_id", None)
    return response


@app.errorhandler(404)
def demo_404(error):
    app.logger.warning(error)
    response = (render_template("flask_404.html"), 404)
    return response


@app.errorhandler(500)
def demo_500(error):
    app.logger.warning(error)
    response = (render_template("flask_500.html", error=error), 500)
    return response


if __name__ == "__main__":
    csrf.init_app(app)
    app.run(host="127.0.0.1", port=8000, debug=False)
