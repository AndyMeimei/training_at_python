import os, sys
from object.TestExec import TE

if __name__ == "__main__":
    path = os.getcwd().replace("\\", "/")
    print(path)
    num = len(sys.argv)
    if num == 4:
        TE.init(path, sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        TE.init(path, "wall-e", "smoke_web", "D:/data/report")
    TE.run()
