class TestSchema:
    def __init__(self, ts):
        self.run_env = ts["run_env"]
        self.run_module = ts["run_module"]
        self.run_level = ts["run_level"]
        self.email = ts["email"]
