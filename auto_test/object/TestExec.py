import pytest
import util.HandleCommon as hc
from object.TestSchema import TestSchema


class TestExec:
    def __init__(self):
        print("初始化执行")

    def init(self, main_path, executor, schema, report_path):
        self.main_path = main_path
        self.executor = executor
        self.schema = schema
        self.report_path = report_path
        self.init_cache()
        self.init_param()
        self.init_schema()
        self.init_env()
        self.init_object()

    def init_cache(self):
        self.cache = {}
        self.cache["test_param"] = hc.get_dict(self.main_path, "/config/test_param.json")
        self.cache["test_schema"] = hc.get_dict(self.main_path, "/config/test_schema/" + self.schema + ".json")
        self.cache["test_env"] = hc.get_dict(self.main_path, "/config/test_env/" + self.cache["test_schema"]["run_env"] + ".json")
        self.cache["test_object"] = self.main_path + "/config/test_object"

    def init_param(self):
        self.PARAM = self.cache["test_param"]

    def init_schema(self):
        ts = self.cache["test_schema"]
        self.SCHEMA = TestSchema(ts)

    def init_env(self):
        env = self.SCHEMA.run_env
        self.ENV = self.cache["test_env"]

    def init_object(self):
        self.OBJECT = self.cache["test_object"]

    def run(self):
        cmds = [self.main_path + "/project/testcase"]
        if len(self.SCHEMA.run_module) != 0:
            cmds[0] += self.SCHEMA.run_module[0]
        cmds.append("-s")
        if len(self.SCHEMA.run_level) != 0:
            for v in self.SCHEMA.run_level:
                cmds.append("-m " + v)
        cmds.append("--html=" + self.report_path + "/pytest.html")
        cmds.append("--alluredir=" + self.report_path + "/alluredir")
        print(cmds)
        pytest.main(cmds)
        # send_email(self.PARAM["email"]["server"], self.PARAM["email"]["user"], self.PARAM["email"]["passwd"], self.SCHEMA.email["to"], self.SCHEMA.email["subject"], self.SCHEMA.email["content"])


TE = TestExec()
