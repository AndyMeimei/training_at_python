import pymysql
import requests
from object.TestExec import TE


def get_conn():
    mysqldb = pymysql.connect(
        host=TE.ENV["mysql"]["host"],
        port=TE.ENV["mysql"]["port"],
        user=TE.ENV["mysql"]["user"],
        passwd=TE.ENV["mysql"]["passwd"],
        database=TE.ENV["mysql"]["database"]
    )
    return mysqldb


def check_service():
    response = requests.get(TE.ENV["service"]["platform"])
    assert response.status_code == 200


def check_mysql():
    mysqldb = get_conn()
    assert mysqldb is not None
