import pytest
import util.HandleCommon as hc
from appium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from appium.webdriver.common.mobileby import By
from object.TestExec import TE


@pytest.fixture(scope="session")
def get_appium():
    desired_caps = {}
    desired_caps['platformName'] = TE.ENV["appium"]["platformName"]
    desired_caps['platformVersion'] = TE.ENV["appium"]["platformVersion"]
    desired_caps['deviceName'] = TE.ENV["appium"]["deviceName"]
    desired_caps['appPackage'] = TE.ENV["appium"]["appPackage"]
    desired_caps['appActivity'] = TE.ENV["appium"]["appActivity"]
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
    driver.implicitly_wait(TE.PARAM["global_delay"])
    return driver


def get_element(driver, pname, vname):
    view = hc.get_dict(TE.OBJECT, "/mobile/" + pname + ".json")[vname]
    cycle_num = TE.PARAM["cycle_num"]
    cycle_delay = TE.PARAM["cycle_delay"]
    if view[0] == "id":
        WebDriverWait(driver, cycle_num, cycle_delay).until(EC.presence_of_element_located((By.ID, view[1])))
        return driver.find_element_by_id(view[1])
    elif view[0] == "xpath":
        WebDriverWait(driver, cycle_num, cycle_delay).until(EC.presence_of_element_located((By.XPATH, view[1])))
        return driver.find_element_by_xpath(view[1])


def is_exist(driver, pname, vname):
    try:
        e = get_element(driver, pname, vname)
        return True
    except:
        return False


def set_text(driver, pname, vname, text):
    e = get_element(driver, pname, vname)
    e.clear()
    e.send_keys(text)


def click(driver, pname, vname):
    e = get_element(driver, pname, vname)
    e.click()


def get_text(driver, pname, vname):
    e = get_element(driver, pname, vname)
    return e.text
