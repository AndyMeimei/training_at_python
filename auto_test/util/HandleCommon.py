import json
import smtplib
from email.mime.text import MIMEText
from email.header import Header


def get_dict(path, file):
    file_path = path + file
    with open(file_path, "r", encoding="UTF-8") as f:
        fdict = json.load(f)
    return fdict


def handle_json(dic_json, conf_json):
    if isinstance(dic_json, dict):
        for key in list(dic_json.keys()):
            if isinstance(dic_json[key], dict):
                handle_json(dic_json[key], conf_json)
            if isinstance(dic_json[key], list):
                handle_json(dic_json[key], conf_json)
            for keyv in list(conf_json.keys()):
                if keyv in dic_json:
                    if dic_json[keyv] != conf_json[keyv]:
                        dic_json[keyv] = conf_json[keyv]
    elif isinstance(dic_json, list):
        for i in range(len(dic_json)):
            handle_json(dic_json[i], conf_json)
    else:
        pass


def handle_null(dic_json):
    if isinstance(dic_json, dict):
        for key in list(dic_json.keys()):
            if isinstance(dic_json[key], dict):
                handle_null(dic_json[key])
            if isinstance(dic_json[key], list):
                handle_null(dic_json[key])
            if dic_json[key] is None:
                del dic_json[key]
    elif isinstance(dic_json, list):
        for i in range(len(dic_json)):
            handle_null(dic_json[i])
    else:
        pass


def send_email(server, user, passwd, to, subject, content):
    msg = MIMEText(content, 'plain', 'utf-8')
    msg['from'] = user
    msg['to'] = to
    msg['Subject'] = Header(subject, 'utf-8')
    smtp = smtplib.SMTP(server)
    smtp.login(user, passwd)
    smtp.sendmail(user, to, msg.as_string())
    smtp.quit()
