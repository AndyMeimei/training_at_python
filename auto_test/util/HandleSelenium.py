import pytest
import util.HandleCommon as hc
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from object.TestExec import TE


@pytest.fixture(scope="session")
def get_selenium():
    options = webdriver.ChromeOptions()
    options.binary_location = TE.ENV["selenium"]["chromeBinary"]
    driver = webdriver.Chrome(TE.ENV["selenium"]["chromeDriver"], options=options)
    driver.implicitly_wait(TE.PARAM["global_delay"])
    driver.maximize_window()
    return driver


def visit(driver, pname):
    page = hc.get_dict(TE.OBJECT, "/web/" + pname + ".json")
    service = page["service"]
    url = TE.ENV["service"][service] + page["url"]
    driver.get(url)


def get_element(driver, pname, vname):
    view = hc.get_dict(TE.OBJECT, "/web/" + pname + ".json")[vname]
    cycle_num = TE.PARAM["cycle_num"]
    cycle_delay = TE.PARAM["cycle_delay"]
    if view[0] == "id":
        WebDriverWait(driver, cycle_num, cycle_delay).until(EC.presence_of_element_located((By.ID, view[1])))
        return driver.find_element_by_id(view[1])
    elif view[0] == "xpath":
        WebDriverWait(driver, cycle_num, cycle_delay).until(EC.presence_of_element_located((By.XPATH, view[1])))
        return driver.find_element_by_xpath(view[1])


def is_exist(driver, pname, vname):
    try:
        unit = get_element(driver, pname, vname)
        return True
    except Exception as e:
        return False


def set_text(driver, pname, vname, text):
    unit = get_element(driver, pname, vname)
    unit.clear()
    unit.send_keys(text)


def click(driver, pname, vname):
    unit = get_element(driver, pname, vname)
    unit.click()


def get_text(driver, pname, vname):
    unit = get_element(driver, pname, vname)
    return unit.text
