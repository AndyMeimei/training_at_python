import requests
import util.HandleCommon as hc
from object.TestExec import TE


def send_post(name, data):
    api = hc.get_dict(TE.OBJECT, "/api/" + name + ".json")
    service = api["service"]
    url = TE.ENV["service"][service] + api["url"]
    request = api["request"]
    hc.handle_json(request, data)
    hc.handle_null(request)
    response = requests.post(url=url, headers=api["header"], data=request)
    print(url)
    print(request)
    print(response.status_code)
    print(response.text)
    return response
