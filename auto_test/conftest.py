import pytest


def pytest_configure(config):
    marker_list = ["smoke"]
    for markers in marker_list:
        config.addinivalue_line("markers", markers)


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    outcome = yield
    report = outcome.get_result()
    report.nodeid = report.nodeid.encode("utf-8").decode("unicode_escape")


@pytest.fixture(scope="session", autouse=True)
def session_init():
    print("start...")
