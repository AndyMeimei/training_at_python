import json, requests


def swagger_data(response):
    datas = response["paths"]
    for key in datas.keys():
        api = {}
        api["path"] = "XXXXX"
        api["path"] = key
        for keyv in datas[key].keys():
            api["method"] = keyv
            spec = datas[key][keyv]
            api["summary"] = spec["summary"].replace("\r", "")
            if "parameters" in spec.keys():
                params = spec["parameters"][0]["schema"]["properties"]
                request = {}
                for keyvv in params.keys():
                    type = params[keyvv]["type"]
                    if type == "string":
                        request[keyvv] = "X"
                    elif type == "number":
                        request[keyvv] = 0
                    elif type == "object":
                        request[keyvv] = {}
                    elif type == "array":
                        request[keyvv] = []
                api["request"] = request
        print(api)


def swagger_file(path):
    with open(path, 'r', encoding='utf8')as f:
        response = json.load(f)
    swagger_data(response)


def swagger_url(url):
    response = requests.get(url).json()
    swagger_data(response)


if __name__ == "__main__":
    path = "d:/360极速浏览器下载/swaggerApi2.json"
    swagger_file(path)
    # url = "http://localhost:8000/apispec_1.json"
    # swagger_url(url)
