import util.HandleAppium as ha


def register():
    pass


def login(driver, username, userpasswd):
    if ha.is_exist(driver, "home", "home_notice"):
        ha.click(driver, "home", "home_notice")
    ha.set_text(driver, "login", "username_edit", username)
    ha.set_text(driver, "login", "userpasswd_edit", userpasswd)
    ha.click(driver, "login", "login_button")
    flag = ha.is_exist(driver, "home", "home_account")
    return flag


def logout():
    pass
