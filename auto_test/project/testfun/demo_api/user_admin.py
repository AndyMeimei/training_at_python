import util.HandleRequest as hr


def register():
    pass


def login(username, userpasswd):
    data = {
        "loginuser_code": username,
        "loginuser_pwd": userpasswd
    }
    result = hr.send_post("login", data)
    flag = (result.status_code == 200)
    return flag


def logout():
    pass
