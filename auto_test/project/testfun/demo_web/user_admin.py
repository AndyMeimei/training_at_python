import util.HandleSelenium as hs


def register():
    pass


def login(driver, username, userpasswd):
    hs.visit(driver, "login")
    hs.set_text(driver, "login", "username_edit", username)
    hs.set_text(driver, "login", "userpasswd_edit", userpasswd)
    hs.click(driver, "login", "login_button")
    flag = hs.is_exist(driver, "home", "frame_up")
    return flag


def logout():
    pass
