import pytest, allure
from project.testfun.demo_web.user_admin import *
from util.HandleSelenium import get_selenium


@allure.feature('用户登录')
@allure.story('使用已存在的正常用户登录系统')
@allure.severity('blocker')
@allure.issue("http://www.jira.com")
@allure.testcase("http://www.testlink.com")
@pytest.mark.smoke
def test_11001(get_selenium):
    driver = get_selenium
    username = "chenjinjun"
    userpasswd = "test1234"
    flag = login(driver, username, userpasswd)
    assert flag is True
