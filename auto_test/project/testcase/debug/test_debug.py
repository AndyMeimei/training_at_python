from object.TestExec import TE
import requests, json


def test_api():
    login_url = TE.ENV["service"]["qfsoft_web_tool"] + TE.OBJECT_API["login"]["url"]
    print("用户登录网址是：", login_url)


def test_web():
    login_url = TE.ENV["service"]["platform"] + TE.OBJECT_WEB["login_page"]["url"]
    print("用户登录网址是：", login_url)


def test_mobile():
    username_edit = TE.OBJECT_MOBILE["login_page"]["username_edit"]
    print("指定的对象属性是：", username_edit)
