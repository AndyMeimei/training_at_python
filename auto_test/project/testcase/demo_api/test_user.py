import pytest, allure
from project.testfun.demo_api.user_admin import *


@allure.feature('用户登录')
@allure.story('使用已存在的正常用户登录系统')
@allure.severity('blocker')
@allure.issue("http://www.jira.com")
@allure.testcase("http://www.testlink.com")
@pytest.mark.smoke
def test_101001():
    username = "guest"
    userpasswd = "test1234"
    flag = login(username, userpasswd)
    assert flag is True
