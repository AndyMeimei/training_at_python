import requests
import json

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_json"
    headers = {"Content-Type": "application/json"}
    body = {"code": "python语言"}
    data = json.dumps(body, ensure_ascii=False).encode("utf-8")
    response = requests.post(url=url, headers=headers, data=data)
    print(response.request.headers)
    print(response.status_code)
    print(json.loads(response.text))
