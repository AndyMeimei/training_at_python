import datetime, requests


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_download"
    response = requests.get(url=url)
    download_name = get_seq()
    download_path = "d:/data" + "/" + download_name
    with open(download_path, "wb") as f:
        f.write(response.content)
    print(response.status_code)
    print(download_path)
