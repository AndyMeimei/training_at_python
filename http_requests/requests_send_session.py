import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url1 = host + "/api/demo_session_set"
    session = requests.session()
    response1 = session.get(url=url1)
    url2 = host + "/api/demo_session_get"
    response2 = session.get(url=url2)
    print(response2.status_code)
    print(response2.text)
    session.close()
