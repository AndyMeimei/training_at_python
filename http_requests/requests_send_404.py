import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/404"
    response = requests.get(url=url)
    print(response.status_code)
    print(response.text)
