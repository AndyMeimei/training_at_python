import datetime, requests


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_cookie_get"
    cookies = {"c_id": get_seq()}
    session = requests.session()
    response = session.get(url=url, cookies=cookies)
    print(response.status_code)
    print(response.text)
    session.close()
