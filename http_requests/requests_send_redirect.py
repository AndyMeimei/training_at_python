import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_redirect"
    params = {"code": "python语言"}
    response = requests.get(url=url, params=params)
    for respv in response.history:
        print(respv.url)
        print(respv.status_code)
        print(respv.headers["location"])
    print(response.text)
