import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_get"
    params = {"code": "error"}
    response = requests.get(url=url, params=params, timeout=3)
    print(response.url)
    print(response.status_code)
    print(response.text)
