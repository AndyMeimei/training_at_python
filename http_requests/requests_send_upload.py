import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_upload"
    upload_path = "d:/data/test1.txt"
    files = {"test_file": ("test1.txt", open(upload_path, "rb"), "text/plain")}
    response = requests.post(url=url, files=files)
    print(response.status_code)
    print(response.text)
