from flask import Flask, current_app
from flask import session, request, make_response, jsonify
from flask_wtf.csrf import CSRFProtect
from flask_cors import cross_origin
from flasgger import Swagger, swag_from
from DBUtils.PooledDB import PooledDB
import random, datetime, time
import gevent.pywsgi as pywsgi
import gevent.monkey as monkey
import pymysql

# monkey.patch_all()
app = Flask("shopping")
app.secret_key = "1234567890"
app.config["JSON_AS_ASCII"] = False
csrf = CSRFProtect()
swagger_config = Swagger.DEFAULT_CONFIG
swagger_config["title"] = "购物网站接口文档"
swagger_config["description"] = "这是一份教学用接口文档"
Swagger(app, config=swagger_config)

pool = PooledDB(
    creator=pymysql,
    maxconnections=10,
    mincached=2,
    maxcached=8,
    maxshared=3,
    host="127.0.0.1",
    user="trainer",
    passwd="test1234",
    database="train_test",
    charset='utf8'
)


def get_id(n):
    v = random.randint(pow(10, n - 1), pow(10, n) - 1)
    return str(v)


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


def query_data(sql):
    current_app.logger.info("查询数据：{}".format(sql))
    conn = pool.connection()
    try:
        cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
        cursor.execute(sql)
        result = cursor.fetchall()
        trans_time()
        cursor.close()
    except Exception as e:
        current_app.logger.info("执行异常：{}".format(e))
    conn.close()
    current_app.logger.info("查询结果：{}".format(result))
    return result


def edit_data(sql):
    current_app.logger.info("更新数据：{}".format(sql))
    conn = pool.connection()
    try:
        cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
        result = cursor.execute(sql)
        trans_time()
        conn.commit()
        cursor.close()
    except Exception as e:
        current_app.logger.info("执行异常：{}".format(e))
        conn.rollback()
    conn.close()
    return result


def edit_data_bat(sqls):
    current_app.logger.info("批量更新数据：{}".format(len(sqls)))
    conn = pool.connection()
    result = 0
    try:
        cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
        for sql in sqls:
            current_app.logger.info("更新数据：{}".format(sql))
            result = result + cursor.execute(sql)
            trans_time()
        conn.commit()
        cursor.close()
    except Exception as e:
        current_app.logger.info("执行异常：{}".format(e))
        conn.rollback()
    conn.close()
    return result


def trans_time():
    num = round(random.random() / 10 + 0.8, 2)
    current_app.logger.info("延时处理：{} 秒".format(str(num)))
    time.sleep(num)


@app.route("/api/register", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/register.yml")
def register():
    trans_time()
    customer_code = request.json.get("customer_code")
    customer_name = request.json.get("customer_name")
    passwd = request.json.get("passwd")
    phone = request.json.get("phone")
    sql = "SELECT * FROM t_shop_customer WHERE customer_code='{}'"
    data = query_data(sql.format(customer_code))
    if len(data) == 0:
        sqlv = "INSERT INTO t_shop_customer(customer_id,customer_code,customer_name,passwd,phone,balance) VALUES(UUID(),'{}','{}','{}','{}','0')"
        edit_data(sqlv.format(customer_code, customer_name, passwd, phone))
        result = jsonify(code=0, message="客户注册成功！")
    else:
        result = jsonify(code=1, message="客户已存在！")
    response = make_response(result)
    return response


@app.route("/api/login", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/login.yml")
def login():
    trans_time()
    customer_code = request.json.get("customer_code")
    passwd = request.json.get("passwd")
    sql = "SELECT * FROM t_shop_customer WHERE customer_code='{}' AND passwd='{}'"
    data = query_data(sql.format(customer_code, passwd))
    if len(data) == 1:
        session["customer_id"] = data[0]["customer_id"]
        result = jsonify(code=0, message="客户鉴权成功！")
    else:
        result = jsonify(code=1, message="客户账号或密码错误！")
    response = make_response(result)
    return response


@app.route("/api/logout", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/logout.yml")
def logout():
    trans_time()
    customer_id = session.get("customer_id")
    if customer_id is not None:
        session.pop("customer_id", None)
        result = jsonify(code=0, message="客户退出成功！")
    else:
        result = jsonify(code=1, message="客户尚未登录！")
    response = make_response(result)
    return response


@app.route("/api/province_list", methods=["GET"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/province_list.yml")
def province_list():
    trans_time()
    sql = "SELECT * FROM t_shop_province WHERE 1=1"
    data = query_data(sql)
    if len(data) == 0:
        result = jsonify(code=2, message="超出列表分页！")
    else:
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


@app.route("/api/city_list", methods=["GET"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/city_list.yml")
def city_list():
    trans_time()
    province_id = request.args.get("province_id", "")
    sql = "SELECT * FROM t_shop_city WHERE province_id='{}'"
    data = query_data(sql.format(province_id))
    if len(data) == 0:
        result = jsonify(code=2, message="超出列表分页！")
    else:
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


@app.route("/api/store_list", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/store_list.yml")
def store_list():
    trans_time()
    city_id = request.json.get("city_id", "")
    keyword = request.json.get("keyword", "")
    page_size = int(request.json.get("page_size", "10"))
    page_index = int(request.json.get("page_index", "1"))
    sql = "SELECT * FROM t_shop_store WHERE city_id='{}' AND store_name LIKE '%{}%' LIMIT {},{}"
    data = query_data(sql.format(city_id, keyword, (page_index - 1) * page_size, page_size))
    if len(data) == 0:
        result = jsonify(code=1, message="超出列表分页！")
    else:
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


@app.route("/api/goods_list", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/goods_list.yml")
def goods_list():
    trans_time()
    store_id = request.json.get("store_id", "")
    keyword = request.json.get("keyword", "")
    page_size = int(request.json.get("page_size", "10"))
    page_index = int(request.json.get("page_index", "1"))
    sql = "SELECT * FROM t_shop_goods WHERE store_id='{}'".format(store_id);
    if len(keyword) > 0:
        sql = sql + " AND goods_name LIKE '%{}%'".format(keyword)
    sql = sql + " LIMIT {},{}".format((page_index - 1) * page_size, page_size)
    data = query_data(sql)
    if len(data) == 0:
        result = jsonify(code=1, message="超出列表分页！")
    else:
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


@app.route("/api/goods_info", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/goods_info.yml")
def goods_info():
    trans_time()
    goods_id = request.json.get("goods_id")
    sql = "SELECT * FROM t_shop_goods WHERE goods_id='{}'"
    data = query_data(sql.format(goods_id))
    time.sleep(random.randint(1, 2))
    if len(data) != 1:
        result = jsonify(code=1, message="商品信息不存在！")
    else:
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


@app.route("/api/goods_buy", methods=["POST"])
@cross_origin()
@csrf.exempt
@swag_from("swagger/goods_buy.yml")
def goods_buy():
    trans_time()
    customer_id = session.get("customer_id")
    if customer_id is not None:
        goods_id = request.json.get("goods_id")
        freight = float(request.json.get("freight"))
        reward = float(request.json.get("reward"))
        quantity = float(request.json.get("quantity"))
        sql = "SELECT * FROM t_shop_goods WHERE goods_id='{}'"
        data1 = query_data(sql.format(goods_id))
        sqls = []
        if len(data1) == 1:
            total_price = data1[0]["unit_price"] * quantity + freight - reward
            sqlv1 = "UPDATE t_shop_goods SET inventory=inventory-{} WHERE goods_id='{}' AND inventory>{}"
            sqls.append(sqlv1.format(quantity, goods_id, quantity))
            sqlv2 = "UPDATE t_shop_customer SET balance=balance-{} WHERE customer_id='{}'"
            sqls.append(sqlv2.format(total_price, customer_id))
            sqlv3 = "INSERT INTO t_shop_order VALUES (UUID(),'{}','{}','{}','{}','{}','{}','{}',NOW())"
            sqls.append(sqlv3.format(customer_id, goods_id, data1[0]["unit_price"], quantity, freight, reward, total_price))
            data2 = edit_data_bat(sqls)
            if data2 == 3:
                result = jsonify(code=0, message="数据处理成功！")
            else:
                result = jsonify(code=3, message="商品库存不足！")
        else:
            result = jsonify(code=2, message="商品不存在！")
    else:
        result = jsonify(code=1, message="客户尚未登录！")
    response = make_response(result)
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
    # gevent_server = pywsgi.WSGIServer(('0.0.0.0', 8000), app)
    # gevent_server.serve_forever()
