import json, random, datetime, logging
from django.http import HttpResponse, JsonResponse, StreamingHttpResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render, redirect, reverse


def get_id(n):
    v = random.randint(pow(10, n - 1), pow(10, n) - 1)
    return str(v)


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


def file_iterator(file_name, chunk_size=512):
    with open(file_name, 'r', encoding='utf-8') as f:
        while True:
            c = f.read(chunk_size)
            if c:
                yield c
            else:
                break


@require_http_methods(["GET"])
def demo_get(request):
    logging.warning("发起请求(GET)：/api/demo_get")
    code = request.GET.get("code", "")
    if code == "error":
        raise Exception("系统异常错误")
    result = "code = " + code
    response = HttpResponse(result)
    return response


@require_http_methods(["POST"])
def demo_post(request):
    code = request.POST.get("code")
    result = "code = " + code
    response = HttpResponse(result)
    return response


@require_http_methods(["POST"])
def demo_json(request):
    code = json.loads(request.body).get("code")
    result = {"code": code}
    response = JsonResponse(result, content_type="application/json;charset=utf-8")
    return response


@require_http_methods(["POST"])
def demo_upload(request):
    test_file = request.FILES.get("test_file")
    path = "D:/lab/python/pycharm/http_django/static/"
    upload_ext = test_file.name.split(".")[-1]
    upload_name = get_seq() + get_id(3) + "." + upload_ext
    upload_file = open(path + upload_name, "wb")
    for line in test_file.chunks():
        upload_file.write(line)
    upload_file.close()
    result = "success"
    response = HttpResponse(result)
    return response


@require_http_methods(["GET"])
def demo_download(request):
    path = "d:/data/test1.txt"
    download_ext = path.split(".")[-1]
    download_name = get_seq() + get_id(3) + "." + download_ext
    response = StreamingHttpResponse(file_iterator(path))
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(download_name)
    return response


@require_http_methods(["GET"])
def demo_template(request):
    code = request.GET.get("code")
    result = {"code": code}
    response = render(request, "django_template.html", result)
    return response


@require_http_methods(["GET"])
def demo_redirect(request):
    code = request.GET.get("code")
    url = reverse("demo_get") + "?code=" + code
    response = redirect(url)
    return response


@require_http_methods(["GET"])
def demo_cookie_get(request):
    c_id = request.COOKIES.get("c_id")
    if c_id is None:
        raise Exception("不存在的关键字段")
    result = "get(c_id) = " + c_id
    response = HttpResponse(result)
    return response


@require_http_methods(["GET"])
def demo_cookie_set(request):
    c_id = get_id(8)
    result = "set(c_id) = " + c_id
    response = HttpResponse(result)
    response.set_cookie("c_id", c_id, max_age=3600)
    return response


@require_http_methods(["GET"])
def demo_cookie_reset(request):
    result = "reset cookie"
    response = HttpResponse(result)
    response.delete_cookie("c_id")
    return response


@require_http_methods(["GET"])
def demo_session_get(request):
    s_id = request.session.get("s_id")
    if s_id is None:
        raise Exception("不存在的关键字段")
    result = "get(s_id) = " + s_id
    response = HttpResponse(result)
    return response


@require_http_methods(["GET"])
def demo_session_set(request):
    s_id = get_id(8)
    result = "set(s_id) = " + s_id
    response = HttpResponse(result)
    request.session["s_id"] = s_id
    return response


@require_http_methods(["GET"])
def demo_session_reset(request):
    result = "reset session"
    response = HttpResponse(result)
    request.session.pop("s_id", None)
    return response


def demo_404(request, exception):
    logging.warning(exception)
    response = render(request, "django_404.html", status=404)
    return response


def demo_500(request):
    response = render(request, "django_500.html", status=500)
    return response
