import json, random, datetime, logging
from django.http import HttpResponse, JsonResponse, StreamingHttpResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render, redirect, reverse


@require_http_methods(["GET"])
def get_provinces(request):
    logging.warning("发起请求(GET)：/api/get_provinces")
    provinces = [{"province_code": "hubei", "province_name": "湖北"}]
    result = {"code": 0, "message": "success", "data": provinces}
    response = JsonResponse(result, content_type="application/json;charset=utf-8")
    return response


@require_http_methods(["GET"])
def get_citys(request):
    logging.warning("发起请求(GET)：/api/get_citys")
    province_code = request.GET.get("province_code", "")
    if len(province_code) == 0:
        result = {"code": 101, "message": "没有数据"}
    else:
        citys = [{"city_code": "wuhan", "city_name": "武汉"}, {"city_code": "huangshi", "city_name": "黄石"}]
        result = {"code": 0, "message": "success", "data": citys}
    response = JsonResponse(result, content_type="application/json;charset=utf-8")
    return response


@require_http_methods(["POST"])
def get_shops(request):
    logging.warning("发起请求(POST)：/api/get_shops")
    city_code = json.loads(request.body).get("city_code", "")
    if len(city_code) == 0:
        result = {"code": 101, "message": "没有数据"}
    else:
        shops = [{"shop_code": "S001", "shop_name": "汉口武商广场店"}, {"shop_code": "S002", "shop_name": "武昌杨汊湖店"}]
        result = {"code": 0, "message": "success", "data": shops}
    response = JsonResponse(result, content_type="application/json;charset=utf-8")
    return response
