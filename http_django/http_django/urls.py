"""http_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from web_myapp import views_demo
from web_myapp import views_ajax

urlpatterns = [
    path('admin/', admin.site.urls),
    url('api/demo_get', views_demo.demo_get, name='demo_get'),
    url('api/demo_post', views_demo.demo_post),
    url('api/demo_json', views_demo.demo_json),
    url('api/demo_upload', views_demo.demo_upload),
    url('api/demo_download', views_demo.demo_download),
    url('api/demo_template', views_demo.demo_template, name="django_template"),
    url('api/demo_redirect', views_demo.demo_redirect),
    url('api/demo_cookie_get', views_demo.demo_cookie_get),
    url('api/demo_cookie_set', views_demo.demo_cookie_set),
    url('api/demo_cookie_reset', views_demo.demo_cookie_reset),
    url('api/demo_session_get', views_demo.demo_session_get),
    url('api/demo_session_set', views_demo.demo_session_set),
    url('api/demo_session_reset', views_demo.demo_session_reset),
    url('api/get_provinces', views_ajax.get_provinces),
    url('api/get_citys', views_ajax.get_citys),
    url('api/get_shops', views_ajax.get_shops)
]

handler404 = views_demo.demo_404
handler500 = views_demo.demo_500
