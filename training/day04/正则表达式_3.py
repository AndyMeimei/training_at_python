#!/usr/bin/python3
import re

if __name__ == "__main__":
    sen = "There are 123,456,789 rmb, please give to me!"
    p = re.compile("\d+,\d+?")
    for com in p.finditer(sen):
        mm = com.group()
        sen = sen.replace(mm, mm.replace(",", ""))
    print(sen)
