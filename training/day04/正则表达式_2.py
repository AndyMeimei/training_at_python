#!/usr/bin/python3
import re


def expand_abbr(sen, abbr):
    lenabbr = len(abbr)
    ma = ''
    for i in range(0, lenabbr):
        ma += abbr[i] + "[a-z]+" + ' '
    print('ma:', ma)
    ma = ma.strip(' ')
    p = re.search(ma, sen)
    if p:
        return p.group()
    else:
        return ""


if __name__ == "__main__":
    s = expand_abbr("Welcome to our Algriculture Bank China and join it.", "ABC")
    print(s)
