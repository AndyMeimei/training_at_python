#!/usr/bin/python3
import threading
import time
import datetime


def print_time(name, delay, counter):
    while counter:
        time.sleep(delay)
        print("%s: %s" % (name, datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f")))
        counter -= 1


class MyThread(threading.Thread):
    def __init__(self, idv, namev, counter):
        threading.Thread.__init__(self)
        self.idv = idv
        self.namev = namev
        self.counter = counter

    def run(self):
        print("开始线程：" + self.namev)
        threadLock.acquire()
        print_time(self.namev, self.counter, 3)
        threadLock.release()
        print("退出线程：" + self.namev)


if __name__ == "__main__":
    threadLock = threading.Lock()
    threads = [MyThread(1, "Thread-1", 1), MyThread(2, "Thread-2", 2)]
    for t in threads:
        t.start()
    for t in threads:
        t.join()
