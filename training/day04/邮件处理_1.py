#!/usr/bin/python3
import smtplib
from email.mime.text import MIMEText
from email.header import Header

if __name__ == "__main__":
    server = '192.168.6.169'
    username = 'trainer@qfsoft.top'
    password = 'test1234'

    sender = 'trainer@qfsoft.top'
    receiver = 'tester1@qfsoft.top'
    subject = '测试邮件2020'
    msg = MIMEText('你好，这是一封测试用的邮件！', 'plain', 'utf-8')
    msg['from'] = 'trainer@qfsoft.top'
    msg['to'] = 'tester1@qfsoft.top'
    msg['Subject'] = Header(subject, 'utf-8')

    smtp = smtplib.SMTP(server)
    smtp.login(username, password)
    smtp.sendmail(sender, receiver, msg.as_string())
    smtp.quit()
