#!/usr/bin/python3
import mysql.connector

if __name__ == "__main__":
    mydb = mysql.connector.connect(
        host="localhost",
        user="trainer",
        passwd="123456",
        database="qfsoft_db_training"
    )
    mycursor = mydb.cursor()
    sql1 = "SELECT * FROM t_school_student WHERE student_name LIKE '李%'"
    mycursor.execute(sql1)
    myresult = mycursor.fetchall()
    for x in myresult:
        print(x)
    sql_select2 = "t2.student_code,t2.student_name,COUNT(1) AS total_num,ROUND(AVG(t1.result),2) AS avg_score"
    sql_from2 = "t_school_score t1 INNER JOIN t_school_student t2 ON t2.student_code=t1.student_code"
    sql_group2 = "t2.student_code HAVING AVG(t1.result)>=60"
    sql2 = "SELECT " + sql_select2 + " FROM " + sql_from2 + " GROUP BY " + sql_group2
    mycursor.execute(sql2)
    myresult = mycursor.fetchall()
    for x in myresult:
        print(x)
    for x in myresult:
        student_code = x[0]
        student_name = x[1]
        total_num = x[2]
        avg_score = x[3]
        expv = "INSERT INTO t_school_statics(statics_id, student_code, student_name, total_num, avg_score) VALUES(UUID(), '{}', '{}', '{}', '{}')"
        sqlv = expv.format(student_code, student_name, total_num, avg_score)
        print(sqlv)
        mycursor.execute(sqlv)
    mydb.commit()
    mycursor.close()
    mydb.close()
