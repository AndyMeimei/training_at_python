#!/usr/bin/python3
import mysql.connector

if __name__ == "__main__":
    mydb = mysql.connector.connect(
        host="localhost",
        user="trainer",
        passwd="123456",
        database="qfsoft_db_training"
    )
    mycursor = mydb.cursor()
    try:
        mycursor.execute("INSERT INTO t_site(site_id, site_name, site_url, memo) VALUES(UUID(), 'test1', 'http://www.test1.com', '')")
        mycursor.execute("INSERT INTO t_site(site_id, site_name, site_url, memo) VALUES(UUID(), 'test2', 'http://www.test2.com', '')")
        mycursor.execute("UPDATE t_site SET site_name='test' WHERE site_name='test1'")
        mycursor.execute("DELETE FROM t_site WHERE site_name='test'")
        mydb.commit()
    except:
        mydb.rollback()
    mycursor.execute("SELECT * FROM t_site")
    myresult = mycursor.fetchall()
    for x in myresult:
        print(x)
    mycursor.close()
    mydb.close()
