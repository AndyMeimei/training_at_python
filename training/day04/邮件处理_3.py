#!/usr/bin/python3
import imaplib
from email.parser import HeaderParser

if __name__ == '__main__':
    server = "localhost"
    username = 'tester1@qfsoft.top'
    password = 'test1234'

    imap = imaplib.IMAP4(server)
    imap.login(username, password)
    result, message = imap.select()
    typeq, data = imap.search(None, 'ALL')
    data = imap.fetch('1', '(BODY[HEADER])')
    header_data = data[1][0][1].decode('utf-8')
    parser = HeaderParser()
    msg = parser.parsestr(header_data)
    print(msg)
    imap.logout()
