#!/usr/bin/python3
import re

if __name__ == "__main__":
    s = input("请输入字符串？")
    flag1 = re.search('^[a-z]+$', s)
    flag2 = re.match('[a-z]+$', s)
    if flag1:
        print("全为小写：真")
    else:
        print("全为小写：假")
    if flag2:
        print("全为小写：真")
    else:
        print("全为小写：假")
