#!/usr/bin/python3
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import requests

if __name__ == "__main__":
    server = 'localhost'
    username = 'admin@qfsoft.top'
    password = 'admin1234'

    sender = 'admin@qfsoft.top'
    receiver = 'tester1@qfsoft.top'
    url = "http://www.qfsoft.top"
    response = requests.request(method="GET", url=url)
    code = response.status_code
    subject = '网站检测邮件2020'
    body = "{} -> {}".format(url, code)
    msg = MIMEText(body, 'plain', 'utf-8')
    msg['from'] = 'admin@qfsoft.top'
    msg['to'] = 'tester1@qfsoft.top'
    msg['Subject'] = Header(subject, 'utf-8')

    smtp = smtplib.SMTP(server)
    smtp.login(username, password)
    smtp.sendmail(sender, receiver, msg.as_string())
    smtp.quit()
