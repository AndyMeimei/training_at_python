#!/usr/bin/python3
import threading
import threadpool
import time


def ps(nums):
    for num in nums:
        p(num)


def p(num):
    print(num)


if __name__ == "__main__":
    nums = list(range(100))
    total = 20

    print("实现1")
    threads = []
    for i in range(5):
        t = threading.Thread(target=ps, args=(nums[i * total:(i + 1) * total],))
        t.start()
    while threading.active_count() > 1:
        time.sleep(1)

    print("实现2")
    pool = threadpool.ThreadPool(total)
    reqs = threadpool.makeRequests(p, nums)
    [pool.putRequest(req) for req in reqs]
    pool.wait()
