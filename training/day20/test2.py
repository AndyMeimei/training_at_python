import random, datetime
from flask import Flask, request, make_response, jsonify
from flask_wtf.csrf import CSRFProtect

app = Flask("flask_demo")
app.secret_key = "1234567890"
csrf = CSRFProtect()


def get_id(n):
    v = random.randint(pow(10, n - 1), pow(10, n) - 1)
    return str(v)


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


@app.route("/api/demo_json", methods=["POST"])
@csrf.exempt
def demo_json():
    code = request.json.get("code")
    result = jsonify(code1=code + "-test1", code2=code + "-test2")
    response = make_response(result)
    return response


if __name__ == "__main__":
    csrf.init_app(app)
    app.run(host="0.0.0.0", port=8000, debug=False)
