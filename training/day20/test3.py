import pymysql


def query_data(sql):
    print(sql)
    mydb = pymysql.connect(
        host="127.0.0.1",
        user="trainer",
        passwd="test1234",
        database="train_test"
    )
    mycursor = mydb.cursor(cursor=pymysql.cursors.DictCursor)
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print(myresult)
    return myresult


def gen_data(sql, fname):
    rows = query_data(sql)
    path = "d:/data"
    f = open(path + "/" + fname, "w")
    datas = []
    for row in rows:
        data = ""
        for v in row.keys():
            data = data + row[v] + ","
        dv = data[0:len(data) - 1] + "\n"
        datas.append(dv)
    f.writelines(datas)
    f.close()


if __name__ == '__main__':
    sql1 = "SELECT customer_code FROM t_shop_customer WHERE 1=1 AND balance>10000"
    gen_data(sql1, "用户参数文件.csv")
    sql2 = "SELECT goods_code FROM t_shop_goods WHERE 1=1 AND quantity>1000"
    gen_data(sql2, "商品参数文件.csv")
