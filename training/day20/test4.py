import pymysql, random


def query_data(sql):
    print(sql)
    mydb = pymysql.connect(
        host="127.0.0.1",
        user="trainer",
        passwd="test1234",
        database="train_test"
    )
    mycursor = mydb.cursor(cursor=pymysql.cursors.DictCursor)
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print(myresult)
    return myresult


def edit_data(sql):
    print("更新数据：{}".format(sql))
    mydb = pymysql.connect(
        host="127.0.0.1",
        user="trainer",
        passwd="test1234",
        database="train_test"
    )
    mycursor = mydb.cursor(cursor=pymysql.cursors.DictCursor)
    myresult = mycursor.execute(sql)
    mydb.commit()
    mycursor.close()
    mydb.close()
    return myresult


def gen_customer():
    sql = "INSERT INTO t_shop_customer(customer_id,customer_code,customer_name,passwd,phone,balance) VALUES (UUID(),'C{}','测试客户{}','123456','1801111{}','99999')"
    for i in range(1, 10):
        customer_pos = i + 1000
        edit_data(sql.format(customer_pos, customer_pos, customer_pos))


def gen_goods():
    data = query_data("SELECT * FROM t_shop_store WHERE 1=1")
    sql = "INSERT INTO t_shop_goods(goods_id,store_id,goods_name,unit_price,quantity,freight,reward) VALUES (UUID(),'{}','测试商品{}','{}','9999','{}','{}')"
    for i in range(1, 100):
        store_id = data[i % 5]["store_id"]
        goods_pos = i + 1000
        unit_price = round(random.random() * 100 + 2, 2)
        freight = round(unit_price * 0.1, 2)
        reward = round(unit_price * 0.05, 2)
        edit_data(sql.format(store_id, goods_pos, unit_price, freight, reward))


if __name__ == '__main__':
    # gen_customer()
    gen_goods()
