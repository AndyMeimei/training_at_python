#!/usr/bin/python3

if __name__ == "__main__":
    list1 = [123, 98, 3082, 98, 2000, 9999]
    list1.pop()
    tuple1 = tuple(list1)
    print("新元组的最大值：", max(tuple1))
    print("新元组的最小值：", min(tuple1))
    x = tuple1[2 - 1] - tuple1[5 - 1]
    print("第2个元素减去第5个元素的值：%d" % x)
