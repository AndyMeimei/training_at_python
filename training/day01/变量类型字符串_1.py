#!/usr/bin/python3

if __name__ == "__main__":
    word = '''Don't aim for Success if you want it; just do what you love and believe in, and it will come naturally.
    I can because i think i can.'''
    print("第5位到23位：%s" % word[5 - 1:23])
    print("you都替换为YOU：%s" % word.replace("you", "YOU"))
    print("是否同时包含success和think：%s" % ((word.lower().find("success") != -1) and (word.lower().find("think") != -1)))
    print(word.lower().find("success"))
