#!/usr/bin/python3

if __name__ == "__main__":
    dict1 = {"id": "L001", "name": "胡一刀", "sex": "男", "class": "315", "score": {"语文": 98, "数学": 93, "英语": 99}}
    print("英语成绩：", dict1["score"]["英语"])
    dict1["score"]["体育"] = 86
    print("添加了体育成绩：", dict1)
    print("是否包含age：", ("age" in dict1))
