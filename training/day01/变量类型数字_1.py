#!/usr/bin/python3
import math

if __name__ == "__main__":
    r = 6378.1 * 1000
    s = 4 * math.pi * math.pow(r, 2)
    v = 3 / 4 * math.pi * math.pow(r, 3)
    print("地球的表面积是：%.2f" % s)
    print("地球的体积是：%.2f" % v)
