#!/usr/bin/python3

if __name__ == "__main__":
    basket1 = {'apple', 'orange', 'berry', 'pear', 'orange', 'banana'}
    basket2 = {'orange', 'berry', 'mike', 'book'}
    print("两个集合的交集是：", (basket1 & basket2))
    print("两个集合的并集是：", (basket1 | basket2))
