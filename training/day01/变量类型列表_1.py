#!/usr/bin/python3

if __name__ == "__main__":
    list1 = [123, 'Google', 'Runoob', 'Taobao', 'Facebook']
    list2 = ['physics', 'chemistry', 1997, 2000]
    list1.remove(123)
    # del list1[0]
    list2.remove(1997)
    list2.remove(2000)
    list3 = list1 + list2
    print("新的列表：", list3)
    list3.sort(reverse=True)
    print("新的列表降序：", list3)
