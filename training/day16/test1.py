from flask import Flask, request, make_response, jsonify, session

app = Flask("flask_test")
app.secret_key = "test1234"


@app.route("/api/demo_get", methods=["GET"])
def demo_get():
    owner = request.args.get("owner", "tom")
    if owner == "zhangsan":
        result = "pass"
    else:
        result = "fail"
    response = make_response(result)
    return response


@app.route("/api/demo_post", methods=["POST"])
def demo_post():
    owner1 = request.args.get("owner", "tom")
    owner2 = request.form.get("owner", "tom")
    if owner1 == "zhangsan" and owner2 == "zhangsan":
        result = "pass"
    else:
        result = "fail"
    response = make_response(result)
    return response


@app.route("/api/demo_json", methods=["POST"])
def demo_json():
    owner1 = request.json.get("owner", "tom")
    if owner1 == "zhangsan":
        result = "pass"
    else:
        result = "fail"
    result = jsonify(result=result)
    response = make_response(result)
    return response


@app.route("/api/demo_upload", methods=["POST"])
def demo_upload():
    test_file = request.files.get("test_file")
    path = "E:/server"
    upload_name = test_file.filename
    upload_path = path + "/" + upload_name
    test_file.save(upload_path)
    return "success"


@app.route("/api/demo_cookie_get", methods=["GET"])
def demo_cookie_get():
    c_id = request.cookies.get("c_id")
    result = ""
    if c_id is not None:
        result = "get(c_id) = " + c_id
    response = make_response(result)
    return response


@app.route("/api/demo_cookie_set", methods=["GET"])
def demo_cookie_set():
    owner = request.args.get("owner", "tom")
    result = "set(c_id) = " + owner
    response = make_response(result)
    response.set_cookie("c_id", owner, max_age=3600)
    return response


@app.route("/api/demo_session_get", methods=["GET"])
def demo_session_get():
    s_id = session.get("s_id")
    result = ""
    if s_id is not None:
        result = "get(s_id) = " + s_id
    response = make_response(result)
    return response


@app.route("/api/demo_session_set", methods=["GET"])
def demo_session_set():
    owner = request.args.get("owner", "tom")
    result = "set(s_id) = " + owner
    response = make_response(result)
    session["s_id"] = owner
    return response


@app.errorhandler(404)
def demo_404(error):
    app.logger.warning(error)
    response = "未找到页面"
    return response


if __name__ == "__main__":
    app.run(debug=True)
