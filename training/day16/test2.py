import mysql.connector
from flask import Flask, request, make_response

app = Flask("flask_test")


def query_data(sql):
    mydb = mysql.connector.connect(
        host="192.168.6.169",
        user="trainer",
        passwd="123456",
        database="train_test"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchone()
    mycursor.close()
    mydb.close()
    return myresult


@app.route("/api/student_num", methods=["GET"])
def student_num():
    data = query_data("SELECT COUNT(*) FROM t_school_student")
    print(data)
    result = str(data[0])
    response = make_response(result)
    return response


if __name__ == "__main__":
    app.run(debug=True)
