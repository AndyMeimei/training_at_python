import mysql.connector
from flask import Flask, request, make_response, jsonify, session

app = Flask("flask_test")
app.secret_key = "1234567890"


def query_data(sql):
    mydb = mysql.connector.connect(
        host="192.168.6.169",
        user="trainer",
        passwd="123456",
        database="train_test"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    return myresult


@app.route("/api/student_login", methods=["POST"])
def student_login():
    student_code = request.json.get("student_code")
    passwd = request.json.get("passwd")
    sql = "SELECT * FROM t_school_student WHERE student_code='" + student_code + "' AND passwd='" + passwd + "'"
    data = query_data(sql)
    print(data)
    result = jsonify(code=1, messgae="用户名或密码错误！")
    if len(data) == 1:
        session["student_id"] = data[0][0]
        result = jsonify(code=0, messgae="用户鉴权成功！")
    response = make_response(result)
    return response


@app.route("/api/student_logout", methods=["POST"])
def student_logout():
    session.pop("student_id", None)
    result = jsonify(code=0, messgae="用户退出成功！")
    response = make_response(result)
    return response


@app.route("/api/student_list", methods=["POST"])
def student_list():
    student_id = session.get("student_id")
    result = jsonify(code=1, messgae="用户尚未登录！")
    if student_id is not None:
        sql = "SELECT * FROM t_school_student WHERE 1=1"
        data = query_data(sql)
        print(data)
        result = jsonify(code=0, data=data)
    response = make_response(result)
    return response


if __name__ == "__main__":
    app.run(debug=True)
