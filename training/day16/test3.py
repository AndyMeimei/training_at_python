import mysql.connector
from flask import Flask, request, make_response, jsonify

app = Flask("flask_test")


def query_data(sql):
    mydb = mysql.connector.connect(
        host="192.168.6.169",
        user="trainer",
        passwd="123456",
        database="train_test"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    return myresult


@app.route("/api/student_score", methods=["POST"])
def student_score():
    student_code = request.json.get("student_code")
    sql = "SELECT course,result FROM t_school_score WHERE student_code='" + student_code + "'"
    data = query_data(sql)
    print(data)
    result = jsonify(code=1, messgae="未找到记录")
    if len(data) != 0:
        scores = []
        for v in data:
            v1 = v[0]
            v2 = v[1]
            scores.append({"course": v1, "score": v2})
        result = jsonify(code=0, scores=scores)
    response = make_response(result)
    return response


if __name__ == "__main__":
    app.run(debug=True)
