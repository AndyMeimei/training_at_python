import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_redirect1"
    params = {"code": "python语言"}
    response = requests.get(url=url, params=params)
    print(response.request.url)
    print(response.status_code)
    print(response.text)
    for respv in response.history:
        print(respv.url)
        print(respv.status_code)
        print(respv.headers["location"])
    assert (response.status_code == 200)
    assert (response.text == "code = python语言")
