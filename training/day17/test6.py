import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_cookie_get"
    cookies = {"c_id": "test1234"}
    response = requests.get(url=url, cookies=cookies)
    print(response.request.url)
    print(response.status_code)
    print(response.text)
    assert (response.status_code == 200)
