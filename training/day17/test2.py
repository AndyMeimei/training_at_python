import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_post"
    data = {"code": "python语言"}
    response = requests.post(url=url, data=data)
    print(response.request.url)
    print(response.status_code)
    print(response.text)
    assert (response.status_code == 200)
    assert (response.text == "code = python语言")
