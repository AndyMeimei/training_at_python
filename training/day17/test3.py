import requests
import json

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_json"
    header = {"Content-Type": "application/json"}
    data = {"code": "python语言"}
    response = requests.post(url=url, headers=header, data=json.dumps(data, ensure_ascii=False).encode("utf-8"))
    print(response.request.url)
    print(response.status_code)
    print(response.text)
    result = json.loads(response.text)
    assert (response.status_code == 200)
    assert (result["code"] == "python语言")
