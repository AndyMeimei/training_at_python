import requests

if __name__ == '__main__':
    host = "http://localhost:8000"
    url = host + "/api/demo_upload"
    files = {"test_file": ("test1.txt", open("d:/data/test1.txt", "rb"), "text/plain")}
    response = requests.post(url=url, files=files)
    print(response.request.url)
    print(response.status_code)
    assert (response.status_code == 200)
