#!/usr/bin/python3


def is_leap(year):
    if (year % 100 != 0 and year % 4 == 0) or (year % 100 == 0 and year % 400 == 0):
        print("是闰年")
    else:
        print("不是闰年")


if __name__ == "__main__":
    s = input("请输入一个年份:")
    n = int(s)
    is_leap(n)
