#!/usr/bin/python3
import math


class Person:
    def __init__(self):
        self.blood = 10000
        self.attack = 300
        self.defend = 100


class Hero(Person):
    def __init__(self, name, extra_attack, extra_defend):
        super().__init__()
        self.name = name
        self.extra_attack = extra_attack
        self.extra_defend = extra_defend

    def get_power(self):
        result = self.blood + (self.defend + self.extra_defend)
        return result


def attack_hero(hero1, hero2):
    p1 = hero1.get_power()
    p2 = hero2.get_power()
    result1 = math.ceil(p1 / (hero2.attack + hero2.extra_attack))
    result2 = math.ceil(p2 / (hero1.attack + hero1.extra_attack))
    print("抗击能力：{} vs {}".format(result1, result2))
    return result1 - result2


if __name__ == "__main__":
    h1 = Hero("亚瑟", 500, 1100)
    h2 = Hero("庄周", 650, 800)
    result = attack_hero(h1, h2)
    win = None
    if result > 0:
        win = h1.name
    elif result < 0:
        win = h2.name
    print("{} vs {} -> 获胜方：{}".format(h1.name, h2.name, win))
