#!/usr/bin/python3
import os


def find_files(path):
    fs = []
    for root, dirs, files in os.walk(path):
        for filev in files:
            path = os.path.join(root, filev)
            fs.append(path)
    return fs


if __name__ == "__main__":
    path = "d:/lab/python/train/zelin"
    fs = find_files(path)
    with open("e:/test.txt", "w") as f:
        for v in fs:
            f.write(v + "\r\n")
        f.close()
