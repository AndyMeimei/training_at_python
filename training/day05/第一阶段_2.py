#!/usr/bin/python3
import random


def get_money(reals, forecasts):
    red = 0
    for ball in forecasts[0:6]:
        if ball in reals[0:6]:
            red += 1
    blue = 0
    if forecasts[6] == reals[6]:
        blue += 1
    print("竞猜分析：{} + {}".format(red, blue))
    money = 0
    if red == 6 and blue == 1:
        money = 500 * 10000
    elif red == 6 and blue == 0:
        money = 250 * 10000
    elif red == 5 and blue == 1:
        money = 3000
    elif red < 5 and blue == 1:
        money = 5
    return money


if __name__ == "__main__":
    real_balls = [3, 8, 11, 9, 22, 31, 8]
    forecast_balls = []
    for i in range(0, 6):
        v = random.randint(1, 33)
        forecast_balls.append(v)
    for i in range(0, 1):
        v = random.randint(1, 16)
        forecast_balls.append(v)
    print("开奖结果：{} + {}".format(real_balls[0:6], real_balls[6]))
    print("随机竞猜：{} + {}".format(forecast_balls[0:6], forecast_balls[6]))
    total = get_money(real_balls, forecast_balls)
    print("总奖金：{}".format(total))
