#!/usr/bin/python3
import re

if __name__ == "__main__":
    str = "Andy is handsome is good man is a player singer actor production! Andy is a good father mother uncle grandfather grandmother."
    datas = str.replace("!", "").replace(".", "").lower().split(" ")
    result = {}
    for v in datas:
        if len(v) == 0:
            continue
        if re.match("[a-z]+", v):
            if v not in result.keys():
                result[v] = 1
            else:
                result[v] += 1
    print("统计结果：{}".format(result))
