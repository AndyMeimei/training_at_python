#!/usr/bin/python3
import random
import threading
import time

total = 0


def save_money(t, m):
    print("存钱：", m)
    t.append(m)
    global total
    total += m


def withdraw_money(t, m):
    print("取钱：", m)
    t.append(0 - m)
    global total
    total -= m


if __name__ == "__main__":
    n = 1000
    total = n
    num_t = random.randint(10, 20)
    num_m1 = random.randint(100, 800)
    num_m2 = random.randint(100, 800)
    threads = []
    trades = []
    for i in range(num_t):
        t1 = threading.Thread(target=save_money, args=(trades, num_m1,))
        t2 = threading.Thread(target=withdraw_money, args=(trades, num_m2,))
        t1.start()
        t2.start()
    num = 100
    while num > 1:
        num = threading.active_count()
        print("目前正在运行的线程数：{}".format(num))
        time.sleep(1)
    print("账户余额：{} -> {}".format(n, total))
    print("对账（存取笔数）：{} -> {}".format(len(trades), num_t * 2))
    print("对账（存取金额）：{} -> {}".format(sum(trades), total - n))
