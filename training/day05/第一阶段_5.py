#!/usr/bin/python3
import time, datetime


def get_week_day(date):
    week_day_dict = {
        0: '星期一',
        1: '星期二',
        2: '星期三',
        3: '星期四',
        4: '星期五',
        5: '星期六',
        6: '星期天',
    }
    day = date.weekday()
    return week_day_dict[day]


if __name__ == "__main__":
    str = "2010-05-03"
    date = datetime.datetime.strptime(str, "%Y-%m-%d")
    weekpos = get_week_day(date)
    print("{} 是 {}".format(str, weekpos))
