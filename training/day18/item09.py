import datetime
import requests, json


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


if __name__ == '__main__':
    host = "http://localhost:8000"
    session = requests.session()

    url = host + "/api/login"
    header = {"Content-Type": "application/json"}
    data = {"user_name": "tester", "user_passwd": "test1234"}
    response = session.post(url=url, headers=header, data=json.dumps(data, ensure_ascii=False).encode("utf-8"))
    print(response.status_code)
    print(json.loads(response.text))

    url = host + "/api/student_list"
    header = {"Content-Type": "application/json"}
    data = {}
    response = session.get(url=url, headers=header)
    print(response.status_code)
    print(json.loads(response.text))

    url = host + "/api/student_info"
    header = {"Content-Type": "application/json"}
    data = {"student_code": "315001"}
    response = session.post(url=url, headers=header, data=json.dumps(data, ensure_ascii=False).encode("utf-8"))
    print(response.status_code)
    print(json.loads(response.text))

    url = host + "/api/statics_score"
    header = {"Content-Type": "application/json"}
    data = {}
    response = session.post(url=url, headers=header, data=json.dumps(data, ensure_ascii=False).encode("utf-8"))
    print(response.status_code)
    print(json.loads(response.text))

    url = host + "/api/download_csv"
    header = {"Content-Type": "application/json"}
    data = {}
    response = session.get(url=url, headers=header)
    download_name = get_seq() + ".csv"
    download_path = "d:/data" + "/" + download_name
    with open(download_path, "wb") as f:
        f.write(response.content)
    print(response.status_code)
    print(download_path)

    url = host + "/api/logout"
    header = {"Content-Type": "application/json"}
    data = {}
    response = session.post(url=url, headers=header, data=json.dumps(data, ensure_ascii=False).encode("utf-8"))
    print(response.status_code)
    print(json.loads(response.text))

    session.close()
