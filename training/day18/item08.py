import random, datetime, time
from flask import Flask, request, make_response, jsonify, send_file
from flask import session
from flask_wtf.csrf import CSRFProtect
from flasgger import Swagger, swag_from
import mysql.connector
import csv


def get_id(n):
    v = random.randint(pow(10, n - 1), pow(10, n) - 1)
    return str(v)


def get_seq():
    v = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return v


def query_data(sql):
    print(sql)
    mydb = mysql.connector.connect(
        host="www.qfsoft.top",
        user="trainer",
        passwd="test1234",
        database="train_test"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print(myresult)
    return myresult


def edit_data(sql):
    print(sql)
    mydb = mysql.connector.connect(
        host="www.qfsoft.top",
        user="trainer",
        passwd="test1234",
        database="train_test"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    mydb.commit()
    mycursor.close()
    mydb.close()


def write_csv(path, data):
    f = open(path, "w", newline="")
    csv_writer = csv.writer(f)
    csv_writer.writerow(["student_code", "student_name", "total_num", "avg_score"])
    for v in data:
        csv_writer.writerow([v[1], v[2], v[3], v[4]])
    f.close()


app = Flask("flask_demo")
app.secret_key = get_id(16)
app.config["UPLOAD_FOLDER"] = "d:/data"
csrf = CSRFProtect()
swagger_config = Swagger.DEFAULT_CONFIG
swagger_config["title"] = "演示接口文档"
swagger_config["description"] = "这是一份演示用接口文档"
Swagger(app, config=swagger_config)


@app.route("/api/login", methods=["POST"])
def login():
    user_name = request.json.get("user_name")
    user_passwd = request.json.get("user_passwd")
    sql = "SELECT * FROM t_sys_user WHERE user_name='" + user_name + "' AND user_passwd='" + user_passwd + "'"
    data = query_data(sql)
    if len(data) == 1:
        session["user_id"] = data[0][0]
        result = jsonify(code=0, messgae="用户鉴权成功！")
    else:
        result = jsonify(code=1, messgae="用户名或密码错误！")
    response = make_response(result)
    return response


@app.route("/api/logout", methods=["POST"])
def logout():
    user_id = session.get("user_id")
    if user_id is not None:
        session.pop("user_id", None)
        result = jsonify(code=0, messgae="用户退出成功！")
    else:
        result = jsonify(code=1, messgae="用户尚未登录！")
    response = make_response(result)
    return response


@app.route("/api/student_list", methods=["GET"])
def student_list():
    user_id = session.get("user_id")
    if user_id is not None:
        sql = "SELECT * FROM t_school_student WHERE 1=1"
        data = query_data(sql)
        result = jsonify(code=0, data=data)
    else:
        result = jsonify(code=1, messgae="用户尚未登录！")
    response = make_response(result)
    return response


@app.route("/api/student_info", methods=["POST"])
@csrf.exempt
@swag_from("swagger/student_info.yml")
def student_info():
    user_id = session.get("user_id")
    if user_id is None:
        student_code = request.json.get("student_code")
        sql = "SELECT * FROM t_school_student WHERE student_code='" + student_code + "'"
        data = query_data(sql)
        time.sleep(random.randint(1, 2))
        if len(data) != 1:
            result = jsonify(code=2, messgae="学生信息不存在！")
        else:
            result = jsonify(code=0, data=data)
    else:
        result = jsonify(code=1, messgae="用户尚未登录！")
    response = make_response(result)
    return response


@app.route("/api/statics_score", methods=["POST"])
@csrf.exempt
def statics_score():
    user_id = session.get("user_id")
    if user_id is not None:
        edit_data("DELETE FROM t_school_statics WHERE update_user='chenjinjun'")
        sql = "SELECT a.student_code,a.student_name,3 AS total_num,IFNULL(SUM(b.result)/3,0) FROM t_school_student a LEFT JOIN t_school_score b ON b.student_id=a.student_id GROUP BY a.student_id"
        data = query_data(sql)
        for v in data:
            sqlv = "INSERT INTO t_school_statics(statics_id,student_code,student_name,total_num,avg_score,update_user,update_time) VALUES(UUID(),'" + v[0] + "','" + v[1] + "','" + str(
                v[2]) + "','" + str(v[3]) + "','chenjinjun',NOW())"
            edit_data(sqlv)
        result = jsonify(code=0, messgae="数据处理成功！")
    else:
        result = jsonify(code=1, messgae="用户尚未登录！")
    response = make_response(result)
    return response


@app.route("/api/download_csv", methods=["GET"])
def download_csv():
    user_id = session.get("user_id")
    if user_id is not None:
        sql = "SELECT * FROM t_school_statics WHERE update_user='chenjinjun'"
        data = query_data(sql)
        path = "d:/data/test1.csv"
        write_csv(path, data)
        download_ext = path.split(".")[-1]
        download_name = get_seq() + get_id(3) + "." + download_ext
        response = send_file(path, as_attachment=True, attachment_filename=download_name)
    else:
        result = jsonify(code=1, messgae="用户尚未登录！")
        response = make_response(result)
    return response


if __name__ == "__main__":
    app.run(port=8000, debug=True)
