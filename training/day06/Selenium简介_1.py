from selenium import webdriver

if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.binary_location = "D:/project/automation/product/client/GoogleChrome/chrome.exe"
    chrome_driver_binary = "D:/project/automation/product/driver/ChromeDriver_Win_x32.exe"
    driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)
    driver.implicitly_wait(30)
    driver.maximize_window()

    driver.get("https://www.baidu.com")
    keyword = driver.find_element_by_id("kw")
    keyword.send_keys("test")
    search = driver.find_element_by_xpath(".//input[@id='su']")
    search.click()
    txt_nums = driver.find_element_by_xpath(".//span[@class='nums_text']")
    print(txt_nums.text)

    driver.close()
    driver.quit()
