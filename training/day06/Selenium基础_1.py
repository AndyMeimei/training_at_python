from selenium import webdriver
from selenium.webdriver.support.select import Select
import mysql.connector

if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.binary_location = "D:/project/automation/product/client/GoogleChrome/chrome.exe"
    chrome_driver_binary = "D:/project/automation/product/driver/ChromeDriver_Win_x32.exe"
    driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)
    driver.implicitly_wait(30)
    driver.maximize_window()

    driver.get("http://localhost:8080/qfsoft_web_prp/jsp/user/register/register.jsp")
    user_code = driver.find_element_by_name("user_code")
    user_code.send_keys("test1")
    user_name = driver.find_element_by_name("user_name")
    user_name.send_keys("测试者1")
    user_pwd = driver.find_element_by_name("user_pwd")
    user_pwd.send_keys("test1234")
    work_depart = Select(driver.find_element_by_name("work_depart"))
    work_depart.select_by_visible_text("研发中心-测试部")
    gender = Select(driver.find_element_by_name("gender"))
    gender.select_by_visible_text("男")
    phone = driver.find_element_by_name("phone")
    phone.send_keys("18911111111")
    email = driver.find_element_by_name("email")
    email.send_keys("18911111111@189.cn")
    user_type = Select(driver.find_element_by_name("user_type"))
    user_type.select_by_visible_text("内部雇员")
    submit = driver.find_element_by_xpath(".//button[@id='submit_data']")
    submit.click()

    mydb = mysql.connector.connect(
        host="localhost",
        user="trainer",
        passwd="123456",
        database="qfsoft_db_timp"
    )
    mycursor = mydb.cursor()
    sql = "SELECT * FROM t_sys_user WHERE user_code='test1'"
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    if len(myresult) != 1:
        print("功能错误：注册数据有误！")
    mycursor.close()
    mydb.close()

    driver.close()
    driver.quit()
