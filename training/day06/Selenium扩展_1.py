from selenium import webdriver

if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.binary_location = "D:/project/automation/product/client/GoogleChrome/chrome.exe"
    chrome_driver_binary = "D:/project/automation/product/driver/ChromeDriver_Win_x32.exe"
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("window-size=1366x768")
    driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)
    driver.implicitly_wait(30)
    driver.maximize_window()

    stock = "600028"
    driver.get("http://quote.eastmoney.com/sh" + stock + ".html")
    pe = driver.find_element_by_id("gt6").text
    price = driver.find_element_by_id("price9").text
    print("股票{}的市盈率是{}，当前价格是{}".format(stock, pe, price))
    driver.get_screenshot_as_file("d:/" + stock + ".png")

    driver.close()
    driver.quit()
