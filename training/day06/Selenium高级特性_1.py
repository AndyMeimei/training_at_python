from selenium import webdriver
from selenium.webdriver.support.select import Select
import mysql.connector

if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.binary_location = "D:/project/automation/product/client/GoogleChrome/chrome.exe"
    chrome_driver_binary = "D:/project/automation/product/driver/ChromeDriver_Win_x32.exe"
    driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)
    driver.implicitly_wait(30)
    driver.maximize_window()

    driver.get("http://localhost:8080/qfsoft_web_prp/jsp/user/register/register.jsp")
    loginuser_code = driver.find_element_by_name("loginuser_code")
    loginuser_code.send_keys("chenjinjun")
    loginuser_pwd = driver.find_element_by_name("loginuser_pwd")
    loginuser_pwd.send_keys("test1234")
    login = driver.find_element_by_xpath(".//input[@type='submit']")
    login.click()

    driver.switch_to.frame("frame_up")
    工具管理 = driver.find_element_by_xpath(".//span[text()='工具管理']")
    工具管理.click()

    driver.switch_to.default_content()
    driver.switch_to.frame("frame_left")
    工作助手 = driver.find_element_by_xpath(".//ul[text()='工作助手']")
    工作助手.click()
    调查问卷 = driver.find_element_by_xpath(".//ul[@ref='/qfsoft_web_prp/jsp/testfun/prp/tool/assistant/survey/survey_list.jsp']")
    调查问卷.click()

    driver.switch_to.default_content()
    driver.switch_to.frame("frame_rightup")
    发起调查 = driver.find_element_by_xpath(".//a[text()='发起调查']")
    发起调查.click()
    进入调查 = driver.find_element_by_xpath(".//a[text()='进入调查']")
    进入调查.click()

    ws = driver.window_handles
    driver.switch_to.window(ws[-1])
    ta1 = driver.find_element_by_xpath(".//textarea[1]")
    ta1.send_keys("详细评论1")
    ta2 = driver.find_element_by_xpath(".//textarea[2]")
    ta2.send_keys("详细评论2")
    submit = driver.find_element_by_xpath(".//button[@id='submit_data']")
    submit.click()

    driver.close()
    driver.quit()
