#!/usr/bin/python3
import random

if __name__ == "__main__":
    for i in range(0, 10):
        for j in range(1, i + 1):
            print('{}*{}={}'.format(j, i, i * j), end='\n')
            j += 1
        i += 1
