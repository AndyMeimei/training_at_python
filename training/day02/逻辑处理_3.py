#!/usr/bin/python3
import random

if __name__ == "__main__":
    x = int(random.randint(1, 10))
    while True:
        # 让用户输入一个任意数字
        num = int(input("请输入一个数字："))
        if num > 10 or num < 1:
            print('输入有误！')
            continue
        elif num > x:
            print('猜大了!')
            continue
        elif num < x:
            print('猜小了：')
            continue
        elif num == x:
            print('猜对了！')
            break
