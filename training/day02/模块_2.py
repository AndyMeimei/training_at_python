#!/usr/bin/python3
import random
import zelin.demo.day2.模块_1 as mm1

if __name__ == "__main__":
    x = int(random.randint(1, 100))
    y = int(random.randint(1, 100))
    print("{} + {} = {}".format(x, y, mm1.add_num(x, y)))
    print("{} + {} = {}".format(x, y, mm1.add_num(x, y)))
    print("{} - {} = {}".format(x, y, mm1.minus_num(x, y)))
    print("{} * {} = {}".format(x, y, mm1.multi_num(x, y)))
    print("{} / {} = {}".format(x, y, mm1.div_num(x, y)))
    print("加法调用次数：", mm1.total)
