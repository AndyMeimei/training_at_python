import traceback


def callParams(obj, params):
    if len(params) == 0:
        result = obj()
    elif len(params) == 1:
        result = obj(params[0])
    elif len(params) == 2:
        result = obj(params[0], params[1])
    elif len(params) == 3:
        result = obj(params[0], params[1], params[2])
    return result


def moduleFunction(module, methodname, methodparams):
    obj = __import__(module, fromlist=True)
    if hasattr(obj, methodname):
        attrf = getattr(obj, methodname)
        result = callParams(attrf, methodparams)
        return result
    else:
        raise Exception("找不到指定模块下的方法")


def classObject(classpath, classname, classparams):
    classes = __import__(classpath, fromlist=True)
    if hasattr(classes, classname):
        attrc = getattr(classes, classname)
        result = callParams(attrc, classparams)
        return result
    else:
        raise Exception("找不到指定的类")


def classField(classpath, classname, classparams, fieldname):
    classes = __import__(classpath, fromlist=True)
    if hasattr(classes, classname):
        attrc = getattr(classes, classname)
        cls = callParams(attrc, classparams)
        if hasattr(cls, fieldname):
            result = getattr(cls, fieldname)
            return result
        else:
            raise Exception("找不到指定类下的属性")
    else:
        raise Exception("找不到指定的类")


def classFunction(classpath, classname, classparams, methodname, methodparams):
    classes = __import__(classpath, fromlist=True)
    if hasattr(classes, classname):
        attrc = getattr(classes, classname)
        cls = callParams(attrc, classparams)
        if hasattr(cls, methodname):
            attrm = getattr(cls, methodname)
            result = callParams(attrm, methodparams)
            return result
        else:
            raise Exception("找不到指定类下的方法")
    else:
        raise Exception("找不到指定的类")


def getDetail():
    message = traceback.print_exc()
    return message


if __name__ == '__main__':
    try:
        teachers = [[classObject("day02.module_fun", "Tester", ["张三", "数学"]), classObject("day02.module_fun", "Tester", ["李四", "数学"])]]
        listv = moduleFunction("day02.module_fun", "get_list", teachers)
        print(listv)
        name = classField("day02.module_fun", "Tester", ["张三", "数学"], "name")
        print(name)
        infov = classFunction("day02.module_fun", "Tester", ["张三", "数学"], "get_info", [])
        print(infov)
    except Exception as e:
        getDetail()
