#!/usr/bin/python3
import datetime

if __name__ == "__main__":
    today = datetime.date.today()
    pos = today.weekday()
    for i in range(0, 7):
        dayv = today + datetime.timedelta(days=i - pos)
        print(dayv.strftime("%Y/%m/%d"))
