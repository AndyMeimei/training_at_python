#!/usr/bin/python3


def make_shirt(*args):
    size = "L"
    title = "保时捷"
    if len(args) == 2:
        size = args[0]
        title = args[1]
    print("T 恤的尺码是：", size)
    print("T 恤的字样是：", title)


if __name__ == "__main__":
    make_shirt()
    make_shirt("M", "大世界")
