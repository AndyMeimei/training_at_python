#!/usr/bin/python3

def static_string(s):
    digit_number = 0
    alpha_number = 0
    else_number = 0
    for i in s:
        if i.isdigit():  # 检查字符串是否只由数字组成
            digit_number += 1
        elif i.isalpha():  # 检查字符串是否只由字母组成
            alpha_number += 1
        else:
            else_number += 1
    v = {}
    v["digit"] = digit_number
    v["alpha"] = alpha_number
    v["else"] = else_number
    return v


if __name__ == "__main__":
    text = input("请输入一个字符串：")
    print(static_string(text))
