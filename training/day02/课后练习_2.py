#!/usr/bin/python3


def fibonacci(n):
    if n <= 1:
        return n
    result = fibonacci(n - 1) + fibonacci(n - 2)
    return result


if __name__ == "__main__":
    num = 10
    print(fibonacci(num))
