#!/usr/bin/python3


def change_role(d):
    for v in d.keys():
        if v == "role":
            d[v] = "worker"
        if isinstance(d[v], dict):
            change_role(d[v])


if __name__ == "__main__":
    dict1 = {"name": "Jack", "role": "boss", "class": {"name": "Jack", "role": "boss"}}
    print(dict1)
    change_role(dict1)
    print(dict1)
