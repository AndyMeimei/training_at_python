#!/usr/bin/python3
import json


def gen_case(d1, d2):
    if isinstance(d1, dict):
        for v in d1.keys():
            if v in d2.keys():
                d1[v] = d2[v]
            gen_case(d1[v], d2)
    if isinstance(d1, list):
        for v in d1:
            gen_case(v, d2)


if __name__ == "__main__":
    define_api = {"name": "Jack", "role": "老板",
                  "class": [{"name": "Jack", "role": "老板"}, {"name": "Lucy", "role": "秘书"}]}
    define_case = {"name": None}
    print(define_api["class"])
    gen_case(define_api, define_case)
    print(json.dumps(define_api, indent=4, ensure_ascii=False))
