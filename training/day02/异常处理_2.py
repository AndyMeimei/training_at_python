#!/usr/bin/python3
def view_all(s):
    if len(s) == 0:
        raise Exception("数据异常：列表为空！")
    for v in s:
        if not isinstance(v, int):
            raise Exception("数据异常（" + str(v) + "）：不为整型数字！")


if __name__ == "__main__":
    try:
        tuple1 = (123, 5, "sd", 3, True)
        view_all(tuple1)
    except Exception as e:
        print(e)
    else:
        print("没有任何异常！")
