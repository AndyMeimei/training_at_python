#!/usr/bin/python3

if __name__ == "__main__":
    # 让用户输入一个任意数字
    num = int(input("请输入一个数字："))
    # 定义一个变量用来作余数的判断
    i = 2
    # 定义一个标签用来标记是否是质数
    flag = True
    while i < num:
        if num % i == 0:
            flag = False
        i += 1
    if flag:
        print(num, "是质数")
    else:
        print(num, "不是质数")
