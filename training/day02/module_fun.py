def get_list(teachers):
    result = []
    for teacher in teachers:
        result.append(teacher.get_info())
    return result


class Tester:

    def __init__(self, name, collage):
        self.name = name
        self.collage = collage

    def get_info(self):
        info = "老师姓名：{}，所教科目：{}。".format(self.name, self.collage)
        return info
