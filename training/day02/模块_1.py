#!/usr/bin/python3
import random

total = 0


def add_num(a, b):
    v = a + b
    global total
    total += 1
    return v


def minus_num(a, b):
    v = a - b
    return v


def multi_num(a, b):
    v = a * b
    return v


def div_num(a, b):
    v = a / b
    return v


if __name__ == "__main__":
    x = int(random.randint(1, 100))
    y = int(random.randint(1, 100))
    print("{} + {} = {}".format(x, y, add_num(x, y)))
    print("{} - {} = {}".format(x, y, minus_num(x, y)))
    print("{} * {} = {}".format(x, y, multi_num(x, y)))
    print("{} / {} = {}".format(x, y, div_num(x, y)))
