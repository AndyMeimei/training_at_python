#!/usr/bin/python3
import random

if __name__ == "__main__":
    list1 = []
    for i in range(0, 10):
        num = int(random.randint(1, 100))
        list1.append(num)
    print(list1)
    total = 0
    for v in list1:
        if v % 2 == 0:
            total += 1
    print("偶数统计数量：", total)
