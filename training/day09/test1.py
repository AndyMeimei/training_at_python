from appium import webdriver

if __name__ == '__main__':
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '7.0'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.android.calculator2'
    desired_caps['appActivity'] = '.Calculator'
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
    driver.implicitly_wait(10)
    '''
    driver.find_element_by_id("com.android.calculator2:id/digit_1").click()
    driver.find_element_by_id("com.android.calculator2:id/op_add").click()
    driver.find_element_by_id("com.android.calculator2:id/digit_2").click()
    driver.find_element_by_id("com.android.calculator2:id/eq").click()
    result = driver.find_element_by_id("com.android.calculator2:id/formula").text
    print(result)
   
    print(driver.current_activity)
    print(driver.page_source)
    print(driver.contexts)
     '''
    print(driver.find_element_by_id("com.android.calculator2:id/op_add").text)
    print(driver.find_element_by_xpath(".//*[@resource-id='com.android.calculator2:id/op_add']").text)
    driver.quit()
