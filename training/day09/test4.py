from appium import webdriver

if __name__ == '__main__':
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '8.1'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.android.browser'
    desired_caps['appActivity'] = 'com.android.browser.BrowserActivity'
    desired_caps["noReset"] = True
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
    driver.implicitly_wait(10)

    driver.get("http://www.baidu.com")
    driver.find_element_by_xpath(".//a[text()='首页']")
    driver.save_screenshot("d:/test.png")

    driver.quit()
