from appium import webdriver

if __name__ == '__main__':
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '5.1.1'
    desired_caps['deviceName'] = '127.0.0.1:62001'
    desired_caps['appPackage'] = 'com.android.browser'
    desired_caps['appActivity'] = '.BrowserActivity'
    driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
    driver.implicitly_wait(10)

    driver.find_element_by_xpath(".//*[text()='超能医仙']").click()
    driver.find_element_by_id("com.chineseall.reader:id/tv_read")
    driver.swipe(500, 640, 200, 640)
    driver.swipe(500, 640, 200, 640)
    driver.save_screenshot("")

    driver.quit()
