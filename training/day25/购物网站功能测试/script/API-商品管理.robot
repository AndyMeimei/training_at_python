*** Settings ***
Library    RequestsLibrary
Library    DatabaseLibrary
Resource    ../resource/API-公共资源.txt
Resource    ../resource/DB-公共资源.txt
Suite Setup    Run Keywords    API-创建会话    AND    DB-连接数据库
Suite Teardown    Run Keywords    API-关闭会话    AND    DB-关闭数据库

*** Test Cases ***
API-008.输入正确的参数调用店铺列表接口，返回成功且数据正确。
    [Tags]    api-smoke
    ${resp}    API-店铺列表    ${city_id}    ${store_name}
    Should Be Equal As Integers    200    ${resp.status_code}
    Should Be Equal As Integers    0    ${resp.json()["code"]}
    ${result}    DB-查询数据库    SELECT * FROM t_shop_store WHERE city_id='b4556568-c95e-11ea-9f75-40167e421fbb' AND store_name LIKE '%温馨小店X001%' LIMIT 0,10
    Should Be Equal    ${result}    ${resp.json()["data"]}

API-009.输入错误的参数调用店铺列表接口，返回数据为空。
    [Tags]    api-exception
    ${resp}    API-店铺列表    ${city_id}    ${store_name}x
    Should Be Equal As Integers    200    ${resp.status_code}
    Should Be Equal As Integers    1    ${resp.json()["code"]}
    Should Be Equal As Strings    超出列表分页！    ${resp.json()["message"]}
