*** Settings ***
Library    DateTime
Library    SeleniumLibrary
Resource    ../resource/WEB-公共资源.txt
Suite Setup    WEB-打开浏览器
Suite Teardown    WEB-关闭浏览器

*** Test Cases ***
WEB-001.使用正确的账号和密码点击登录按钮操作页面，登录成功。
    [Tags]    web-smoke
    Go To    url=${site}/demo/shop/login.html
    Input Text    id:customer_code    ${customer_code}
    Input Text    id:passwd    ${passwd}
    Click Button    id:submit_data
    Wait Until Element Is Visible    link:退出
    WEB-页面截图

WEB-007.输入城市和关键字后点击”查询“按钮操作页面，返回数据正确且齐全。
    [Tags]    web-smoke
    Go To    url=${site}/demo/shop/store_list.html
    Select From List By Label    id=province_id    ${province_id}
    Select From List By Label    id=city_id    ${city_id}
    Input Text    id:keyword    ${store_name}
    Click Button    id=query_data
    Wait Until Element Is Enabled    id=query_data
    ${count}    Get Element Count    xpath:.//tbody[@id='store_list']/tr
    Should Be Equal As Integers    1    ${count}
    WEB-页面截图
