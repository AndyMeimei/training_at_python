*** Settings ***
Library    RequestsLibrary
Library    DatabaseLibrary
Resource    ../resource/API-公共资源.txt
Resource    ../resource/DB-公共资源.txt
Suite Setup    Run Keywords    API-创建会话    AND    DB-连接数据库
Suite Teardown    Run Keywords    API-关闭会话    AND    DB-关闭数据库

*** Test Cases ***
002.使用正确的账号和密码调用客户登录接口，返回成功。
    [Tags]    api-smoke
    ${resp}    API-客户登录    ${customer_code}    ${passwd}
    Should Be Equal As Integers    200    ${resp.status_code}
    Should Be Equal As Integers    0    ${resp.json()["code"]}
    Should Be Equal As Strings    客户鉴权成功！    ${resp.json()["message"]}

API-003.输入错误的账号和密码调用客户登录接口，返回报错。
    [Tags]    api-exception
    ${resp}    API-客户登录    ${customer_code}    ${passwd}x
    Should Be Equal As Integers    200    ${resp.status_code}
    Should Be Equal As Integers    1    ${resp.json()["code"]}
    Should Be Equal As Strings    客户账号或密码错误！    ${resp.json()["message"]}
