#!/usr/bin/python3
class Oil:
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b

    def __str__(self):
        v = "({},{},{})".format(self.r, self.g, self.b)
        return v

    def __add__(self, other):
        r = int((self.r + other.r) / 2)
        g = int((self.g + other.g) / 2)
        b = int((self.b + other.b) / 2)
        return Oil(r, g, b)

    @staticmethod
    def mix(one, n1, two, n2):
        r = int((one.r * n1 + two.r * n2) / (n1 + n2))
        g = int((one.g * n1 + two.g * n2) / (n1 + n2))
        b = int((one.b * n1 + two.b * n2) / (n1 + n2))
        return Oil(r, g, b)


if __name__ == "__main__":
    paint1 = Oil(125, 225, 38)
    paint2 = Oil(15, 156, 110)
    paint3 = paint1 + paint2
    print(paint3)
    paint4 = Oil.mix(paint1, 2, paint2, 3)
    print(paint4)
