#!/usr/bin/python3
class Tester:
    passv = 60
    goodv = 90

    def __init__(self, fun, auto, perform, other):
        self.fun = fun
        self.auto = auto
        self.perform = perform
        self.other = other

    def get_plan(self):
        plan = {}
        if self.fun < self.passv:
            plan["fun"] = self.passv - self.fun
        if self.auto < self.passv:
            plan["auto"] = self.passv - self.auto
        if self.perform < self.passv:
            plan["perform"] = self.passv - self.perform
        if self.other < self.passv:
            plan["other"] = self.passv - self.other
        person = (self.fun, self.auto, self.perform, self.other)
        if self.fun == max(person):
            plan["fun"] = self.goodv - self.fun
        if self.auto == max(person):
            plan["auto"] = self.goodv - self.auto
        if self.perform == max(person):
            plan["perform"] = self.goodv - self.perform
        if self.other == max(person):
            plan["other"] = self.goodv - self.other
        return plan


if __name__ == "__main__":
    t1 = Tester(fun=65, auto=25, perform=58, other=86)
    plan = t1.get_plan()
    print(plan)
