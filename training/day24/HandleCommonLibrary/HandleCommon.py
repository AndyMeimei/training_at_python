import datetime


class HandleCommon(object):
    def get_dict_value(self, v, k):
        keys = k.split("/")
        value = v
        for key in keys:
            if len(key) == 0:
                continue
            value = value[key]
        return value

    def get_list_value(self, ls, i, k):
        v = ls[int(i)]
        keys = k.split("/")
        value = v
        for key in keys:
            if len(key) == 0:
                continue
            value = value[key]
        return value

    def get_timestamp(self, fmt):
        now = datetime.datetime.now()
        str_time = now.strftime(fmt)
        return str_time


if __name__ == '__main__':
    common = HandleCommon()
    result = common.get_timestamp("%Y%m%d")
    print(result)
