*** Settings ***
Library           SeleniumLibrary

*** Test Cases ***
测试调试1-001
    Set Selenium Implicit Wait    10
    Open Browser    url=http://192.168.6.5/demo/shop/store_list.html    browser=chrome
    Select From List By Label    id=province    湖北
    Select From List By Label    id=city    武汉
    Click Button    id=query_data
    Wait Until Element Is Enabled    id=query_data
    Capture Page Screenshot    filename=selenium_screenshot_001.png
    Close Browser