*** Settings ***
Library           RequestsLibrary

*** Test Cases ***
测试调试2-001
    # 设置请求头 .
    &{header}=    Create Dictionary    Content-Type=application/json
    # 会话别名为 mysite    接口所在服务器域名地址
    Create Session    mysite    http://localhost:8000    headers=${header}
    # 准备请求数据
    &{data}=    Create Dictionary    page_size=10    page_index=1
    # 发送post请求，并用变量接收响应结果
    ${resp}    Post Request    mysite    /api/goods_list    ${data}
    # ${resp} = <Response [200]>   是一个python requests库当中的Response对象。我们需要从这个对象当中，拿到响应的具体数据。
    # 从python对象当中拿数据的方法:${python中Request的表达式}
    # 获取http请求的状态码
    Log    ${resp.status_code}
    # 获取本次的响应数据
    Log    ${resp.text}
    # 断言 - 状态码200
    Should Be Equal As Integers    200    ${resp.status_code}
    # 断言 - 从字典当中取出code的值，与 注册成功  是否相等。
    Should Be Equal As Integers    1    ${resp.json()["code"]}
