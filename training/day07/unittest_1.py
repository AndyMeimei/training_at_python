from unittest_1 import *
import unittest
import math
import HTMLTestRunner


def circle_area(r):
    if r <= 0:
        return -1
    area = round(math.pi * pow(r, 2), 2)
    return area


class TestMulti(unittest.TestCase):
    def setUp(self):
        print("用例初始化")

    def tearDown(self):
        print("用例结束")

    def test_0001(self):
        print("用例0001执行")
        r = 3.2
        expect = round(3.14 * r * r, 2)
        real = circle_area(r)
        self.assertEqual(expect, real)

    def test_0002(self):
        print("用例0002执行")
        r = -1.5
        expect = round(3.14 * r * r, 2)
        real = circle_area(r)
        self.assertEqual(expect, real)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestMulti))
    with open("d:/unittest_1.html", "wb") as f:
        runner = HTMLTestRunner.HTMLTestRunner(stream=f, verbosity=2)
        runner.run(suite)
