from selenium import webdriver
import pytest


@pytest.fixture(scope="session")
# 加标记的作用是只跑一遍
def get_driver():
    options = webdriver.ChromeOptions()
    options.binary_location = "D:/project/automation/product/client/GoogleChrome/chrome.exe"
    chrome_driver_binary = "D:/project/automation/product/driver/ChromeDriver_Win_x32.exe"
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("window-size=1366x768")
    driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)
    driver.implicitly_wait(30)
    driver.maximize_window()
    return driver


def login(driver, name, pwd):
    driver.get("http://www.qfsoft.top/qfsoft_web_prp/jsp/user/login/login.jsp")
    loginuser = driver.find_element_by_name("loginuser_code")
    loginuser.send_keys(name)
    loginuser_pwd = driver.find_element_by_xpath(".//input[@name='loginuser_pwd']")
    loginuser_pwd.send_keys(pwd)
    login = driver.find_element_by_xpath(".//input[@type='submit' and @value ='登录系统']")
    login.click()


def is_exist(driver, xpath):
    try:
        driver.find_element_by_xpath(xpath)
        return True
    except:
        return False


@pytest.mark.smoke
def test_001(get_driver):
    print("用例001执行")
    driver = get_driver
    login(driver, "yihaiyan", "test1234")
    driver.switch_to.frame("frame_up")
    flag = is_exist(driver, ".//div[@id='menu_bar1']")
    driver.switch_to.default_content()
    assert flag == True


def test_002():
    print("用例002执行")
    driver = get_driver
    login(driver, "michell", "test1234")
    flag = is_exist(driver, ".//div[text()='信息提示']")
    assert flag == False


if __name__ == '__main__':
    pytest.main(["pytest_1.py", "--html=d:/pytest_1.html"])
